create or replace view vue_village_sans_sejour as
    select idv from village
        where idv not in (
            select idv from sejour
        )
;

select * from vue_village_sans_sejour;

declare
    iv village.idv%type;
    l_ids sejour.ids%type;
    a village.activite%type;
begin
    traitement2(1, 'NY', 42, iv, l_ids, a);
    dbms_output.put_line('idv '||iv||', ids '||l_ids||', activite '||a);
end;
/